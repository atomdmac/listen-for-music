#!/bin/bash

# Set some environment variables to make sure Python knows to use UTF8 when
# creating filenames.
export PYTHONIOENCODING=utf8
export LANG=en_US.UTF-8

/volume1/@appstore/python3/bin/python3 ./main.py
