import os
import sys
import time
import logging
import zipfile
import configparser
import shutil
import subprocess
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler, FileCreatedEvent, FileModifiedEvent

config = configparser.ConfigParser()
config.read('main.cfg')

watchPath = config.get('main', 'watchPath')
extractionPath = config.get('main', 'extractionPath')
ownerUser = config.get('main', 'ownerUser', fallback=None)
ownerGroup = config.get('main', 'ownerGroup', fallback=None)

class ChangeHandler(FileSystemEventHandler):
    def isInterestingChange(self, event):
        if event.event_type == FileCreatedEvent.event_type or event.event_type == FileModifiedEvent.event_type:
            return True
        return False

    def isMp3(self, event):
        if event.src_path[-3:] == 'mp3':
            return True
        return False

    def dispatch(self, event):
        try:
            self.handleFileChange(event)
        except:
            print("Ran into issues extracting " + event.src_path)

    def handleFileChange(self, event):
        if event.is_directory == True:
            return

        if self.isInterestingChange(event) == False:
            return

        if zipfile.is_zipfile(event.src_path):
            print("Unpacking files for " + event.src_path)
            unpackFiles(event.src_path)
            print("Success!")

        if self.isMp3(event):
            print("Copying MP3 for " + event.src_path)
            moveSingleFile(event.src_path)
            print("Success!")

        print("Done")

def getPathFromFileName(srcFileName):
    def formatPathComponent(string):
        final = string.strip()
        # TODO: Use RegEx so only the end of the last item is searched
        final = final.replace('.zip', '')
        final = final.replace('.mp3', '')
        return final

    baseFileName = os.path.basename(srcFileName)
    pathComponents = list(map(formatPathComponent, baseFileName.split(' - ')))
    destinationPath = os.path.join(extractionPath, pathComponents[0], pathComponents[1])
    return destinationPath

def unpackFiles(srcFileName):
    zipFilePath = os.path.abspath(srcFileName)
    destinationPath = getPathFromFileName(srcFileName)
    os.makedirs(destinationPath, exist_ok=True)

    zipRef = zipfile.ZipFile(zipFilePath, 'r')
    zipRef.extractall(destinationPath)
    zipRef.close()

    setPermissions(destinationPath)

def moveSingleFile(srcFileName):
    destinationPath = getPathFromFileName(srcFileName)
    os.makedirs(destinationPath, exist_ok=True)
    subprocess.check_output(['cp', srcFileName, destinationPath])
    setPermissions(destinationPath)

def setPermissions(directoryPath):
    print("Setting permissions...")
    newOwners = ownerUser + ':' + ownerGroup
    subprocess.check_output(['chown', '-R', newOwners, directoryPath])

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    #  event_handler = LoggingEventHandler()
    event_handler = ChangeHandler()
    observer = Observer()
    observer.schedule(event_handler, watchPath, recursive=False)
    observer.start()

    print("Listening for file changes...")

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
