# listen-for-music

A Python script that:
- Watches for Bandcamp ZIP files to be added to a directory
- Unzips the files
- Creates an `artist/album` directory structure
- Moves the files to the new directory

# Installation

- Clone the repo
- Rename `main.template.cfg` -> `main.cfg`
- Update the values in `main.cfg` to suit your setup

# Start Script on System Start

If using a Synology system:
- Update `listen-for-music.conf` to point to the directory where this repo lives.
- Link or copy `listen-for-music.conf` to `/etc/init`
